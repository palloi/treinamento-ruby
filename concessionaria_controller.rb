require './concessionaria.rb'
require './funcionario_controller.rb'
require './veiculo_controller.rb'
#coding: utf-8

class ConcessionariaController
	def cadastrar(dados)
		concessionaria = Concessionaria.new(dados)

		concessionaria.qtdFuncionarios = self.totalFuncionarios()
		concessionaria.qtdVeiculos = self.totalVeiculos()
		concessionaria.id = (File.open("dados/concessionarias.txt").readlines.length + 1).to_s
		
		File.open("dados/concessionarias.txt", "a") do |f|
			f << "#{concessionaria.to_hash}\n"
		end
	end

	def lista
		concessionarias ||= Array.new
		File.open("dados/concessionarias.txt").each do |l|
			concessionarias.push(Funcionario.to_concessionaria(eval(l)))
		end
		concessionarias
	end

	def buscar_por_nome(nome)
		concessionarias = self.lista()
		concessionarias.select { |e| e if e.nome == nome  }
	end

	def totalFuncionarios
		funcionarios = FuncionarioController.new
		funcionarios.total()
	end

	def totalVeiculos
		veiculos = VeiculoController.new
		veiculos.total()
	end
end