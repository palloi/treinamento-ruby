#coding: utf-8

require './funcionario.rb'
require './new_exceptions.rb'

class FuncionarioController
	def cadastrar(dados)
		funcionario = Funcionario.new(dados)
		
		funcionario.id = (File.open("dados/funcionarios.txt").readlines.length + 1).to_s
		File.open("dados/funcionarios.txt", "a") do |f|
			f << "#{funcionario.to_hash}\n"
		end

	rescue NomeInvalido
		puts NomeInvalido.msg
	end

	def buscar_por_nome(nome)
		funcionarios = self.lista()
		funcionarios.select { |e| e if e.nome == nome  }
	end

	def lista
		funcionarios ||= Array.new
		File.open("dados/funcionarios.txt").each do |l|
			funcionarios.push(Funcionario.to_funcionario(eval(l)))
		end
		funcionarios
	end

	def total
		self.lista().length
	end
end