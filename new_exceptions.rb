#coding: utf-8

class AnoMinimoCarro < StandardError
	def message
		"Não trabalhamos com carros velhos, por favor né!"
	end
end

class NomeInvalido < StandardError
	def message
		"No minimo 2 nomes para cadastrar!"
	end
end