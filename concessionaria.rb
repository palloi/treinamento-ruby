#coding: utf-8

class Concessionaria
	attr_accessor :id, :nome, :cidade, :qtdVeiculos, :qtdFuncionarios, :data

	def initialize(dados)
		self.to_self(dados)
	end

	def to_hash
		{id: self.id, nome: self.nome, cidade: self.cidade, qtdVeiculos: self.qtdVeiculos, qtdFuncionarios: self.qtdFuncionarios, data: self.data}.to_s
	end

	def to_self(dados)
		self.nome = dados[:nome]
		self.cidade = dados[:cidade]
		self.qtdVeiculos = dados[:qtdVeiculos]
		self.qtdFuncionarios = dados[:qtdFuncionarios]
		self.data = Time.now.to_s
		self.id = dados[:id] if dados[:id]
		self
	end

	def self.to_concessionaria(dados)
		self.new(dados)
	end
end