var application = {
	init: function(){
		$('.table-customer .ico-trash').bind('click', application.confirmDelete);
	},

	navOn: function(eq){
		$('#menuPrincipal li').eq(eq).addClass('selected');
	},

	messageDelete: function(name){
		return "Você realmente deseja excluir o customer " + name + "?";
	},

	confirmDelete: function(){
		$('#modal_delete form').attr('action', $(this).attr('href'));
		$('#modal_delete p').text(application.messageDelete($(this).parents('tr').find('td:first').text()));
		$('#modal_delete').modal('show');
		return false
	}
};

$('document').ready(application.init);