helpers do
	STATUS = { :actving => "Actving", :disable => "Disable", :enable => "Enable", :deleted => "Deleted" }
	def status_options(selected=nil)
		options = ""
		STATUS.each do |key, value|
			select = (key == selected.to_sym) ? "selected" : ""
			options << "<option value='#{key}' #{select}>#{value}</option>"
		end
		options
	end
end