#enconding: utf-8

require 'sinatra'
require 'json'
require 'net/http'
require './helpers/helpers'

class Client
	get '/' do
		uri = URI("http://localhost:4567/api/v1/customers")
		res = Net::HTTP.get(uri)

		@result_customer = JSON.parse(res)

		erb :index, :layout => :'layout/application'
	end

	get '/search' do
		erb :search, :layout => :'layout/application'
	end

	post '/search' do
		customer = params[:customer]
		
		uri = URI("http://localhost:4567/api/v1/customers/#{customer[:select]}/#{customer[:value]}")
		res = Net::HTTP.get(uri)

		@result_customer = JSON.parse(res)
		
		erb :search, :layout => :'layout/application'
	end

	get '/new' do
		@title = "New Customer"
		erb :new, :layout => :'layout/application'
	end

	post '/new' do
		@title = "New Customer"
		customer = params[:customer]
		
		uri = URI("http://localhost:4567/api/v1/customers/new")
		res = Net::HTTP.post_form(uri, 'customer[name]' => customer[:name], 'customer[status]' => customer[:status])

		erb :new, :layout => :'layout/application'
	end

	get '/edit/:id' do
		@title = "Edit Customer"

		uri = URI("http://localhost:4567/api/v1/customers/id/#{params[:id]}")
		res = Net::HTTP.get(uri)
		
		@customer = JSON.parse(res)

		erb :new, :layout => :'layout/application'
	end

	post '/edit/:id' do
		@title = "Edit Customer"
		customer = params[:customer]

		begin
			uri = URI("http://localhost:4567/api/v1/customers/#{params[:id]}")

			request = Net::HTTP::Put.new uri.path

			response = Net::HTTP.start(uri.host, uri.port) do |http|
			  request.body = {customer: {name: customer[:name], status: customer[:status]}}.to_json
			  http.request request
			end

		 	redirect '/'
		rescue Exception => e
			redirect '/'
		end
	end

	post '/delete/:id' do
		begin
			uri = URI("http://localhost:4567/api/v1/customers/#{params[:id]}")
			request = Net::HTTP::Delete.new uri.path

			response = Net::HTTP.start(uri.host, uri.port) do |http|
			  http.request request
			end

			redirect '/'
		rescue Exception => e
			redirect '/'
		end
	end
end