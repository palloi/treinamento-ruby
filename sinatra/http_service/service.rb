#enconding: utf-8

require 'sinatra'
require 'sinatra/json'
require 'json'
#require './config_redis'
require './lib/customers'

class Service
	get '/' do
		content_type :json

		json result: {message: "o cheirao morreu ou o charlie brown da bahia?"}
	end

	get '/api/v1/version' do
		content_type :json

		json result: {version: "1.0.0"}
	end

	get '/api/v1/customers' do
		content_type :json

		c = Customers.all

		puts c.inspect
		
		json c
	end

	post '/api/v1/customers/new' do
		content_type :json

		Customers.new(params[:customer][:name], params[:customer][:status])

		c = Customers.all
		
		json c
	end

	put '/api/v1/customers/:id' do
		content_type :json
		#fazer atualizacaoooo
		customer = JSON.parse(request.body.read)['customer']

		puts "PUTPUT #{customer.inspect}"

		json result: "#{Customers.update(params[:id], customer['name'], customer['status'])}"
	end

	delete '/api/v1/customers/:id' do
		content_type :json
		json result: "#{Customers.delete(params[:id])}"
	end

	get '/api/v1/customers/:key/:value' do
		content_type :json

		case params[:key]
		when 'id'
			c = [Customers.find_by_id(params[:value])]
		when 'name'
			c = Customers.find_by_name(params[:value])
		when 'status'
			c = Customers.find_by_status(params[:value])
		end
		
		json c.reject(&:nil?)
	end
end