class Customers
	CUSTOMERS = [{
					id: '1',
					name: 'Palloi Hofmann',
					created_at: '28-03-2013 18:18:42',
					updated_at: '28-03-2013 18:18:42',
					status: 'activing'
				},
				{
					id: '2',
					name: 'Palloi',
					created_at: '28-03-2013 18:18:42',
					updated_at: '28-03-2013 18:18:42',
					status: 'disable'
				},
				{
					id: '3',
					name: 'Mateus Prado',
					created_at: '28-03-2013 18:18:42',
					updated_at: '28-03-2013 18:18:42',
					status: 'activing'
				}]

	ID_INDEX = {:value => 4}

	def self.new(name, status)
		time = Time.new
		CUSTOMERS.push({
					id: ID_INDEX[:value].to_s,
					name: name,
					created_at: time.strftime("%d-%m-%Y %H:%M:%S"),
					updated_at: time.strftime("%d-%m-%Y %H:%M:%S"),
					status: status
				})
		
		ID_INDEX[:value] = ID_INDEX[:value] + 1

		CUSTOMERS
	end

	def self.update(id, name, status)
		c = self.find_by_id(id);
		time = Time.new
		CUSTOMERS.each do |customer|
			if customer[:id] == id
				customer[:name] = name
				customer[:status] = status
				customer[:updated_at] = time.strftime("%d-%m-%Y %H:%M:%S")
			end
		end

		"Atualiado com sucesso"
	end

	def self.delete(id)
		CUSTOMERS.delete(self.find_by_id(id))
	end

	def self.all
		CUSTOMERS
	end

	def self.find_by_id(id)
		CUSTOMERS.find do |c|
			c[:id] == id
		end
	end

	def self.find_by_name(name)
		CUSTOMERS.select do |c|
			/#{Regexp.quote name}/i =~ c[:name]
		end
	end

	def self.find_by_status(status)
		CUSTOMERS.select do |c|
			/#{Regexp.quote status}/i =~ c[:status]
		end
	end
end

#falta delete and put 