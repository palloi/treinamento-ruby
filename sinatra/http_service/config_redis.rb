require 'redis'

module Config
	def self.redis
		Redis.new(:host => 'localhost', :port => 6379)
	end
end