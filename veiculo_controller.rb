#coding: utf-8

require './veiculo.rb'
require './new_exceptions.rb'

class VeiculoController
	def cadastrar(dados)
		veiculo = Veiculo.new(dados)
		
		veiculo.id = (File.open("dados/veiculos.txt").readlines.length + 1).to_s
		File.open("dados/veiculos.txt", "a") do |f|
			f << "#{veiculo.to_hash}\n"
		end

	rescue AnoMinimoCarro => e
		puts e.message
	end

	def buscar_por_id(id)
		veiculos = self.lista()
		veiculos.select { |e| e if e.id == id  }.first
	end

	def lista
		veiculos ||= Array.new
		File.open("dados/veiculos.txt").each do |l|
			veiculos.push(Veiculo.to_veiculo(eval(l)))
		end
		veiculos
	end

	def total
		self.lista().length
	end
end