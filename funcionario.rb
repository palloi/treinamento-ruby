#coding: utf-8

class Funcionario
	attr_accessor :id, :nome, :idade, :cargo, :data	

	def initialize(dados)
		self.to_self(dados)
	end

	def to_hash
		{id: self.id, nome: self.nome, idade: self.idade, cargo: self.cargo, data: self.data}.to_s
	end

	def to_self(dados)
		raise NomeInvalido if dados[:nome].split(" ").length < 2

		self.nome = dados[:nome]
		self.idade = dados[:idade]
		self.cargo = dados[:cargo]
		self.data = Time.now.to_s
		self.id = dados[:id] if dados[:id]
		self
	end

	def self.to_funcionario(dados)
		self.new(dados)
	end
end