Gem::Specification.new do |spec|
	spec.name = 'bergamota'
	spec.version = '0.0.1'
	spec.date = '2013-03-04'
	spec.description = 'A simple Gem desc'
	spec.summary = 'A simple Gem sumary'
	spec.authors = ['Palloi Hofmann", "Mateus Prado']
	spec.email = 'palloi.hofmann@gmail.com'
	spec.files = ['lib/bergamota.rb']
	spec.homepage = 'https://bitbucket.org/palloi/treinamento-ruby'
end