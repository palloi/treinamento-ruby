#coding: utf-8

require './new_exceptions.rb'

class Veiculo
	attr_accessor :id, :nome, :ano, :modelo, :marca, :cor, :data

	def initialize(dados)
		self.to_self(dados)
	end

	def to_hash
		{id: self.id, nome: self.nome, ano: self.ano, modelo: self.modelo, marca: self.marca, cor: self.cor, data: self.data}.to_s
	end

	def to_self(dados)
		raise AnoMinimoCarro if dados[:ano].to_i < 2005

		self.nome = dados[:nome]
		self.ano = dados[:ano]
		self.modelo = dados[:modelo]
		self.marca = dados[:marca]
		self.cor = dados[:cor]
		self.data = Time.now.to_s
		self.id = dados[:id] if dados[:id]
		self
	end

	def self.to_veiculo(dados)
		self.new(dados)
	end
end